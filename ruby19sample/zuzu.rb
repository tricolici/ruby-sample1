#!/usr/bin/env ruby

require 'net/http'

res = Net::HTTP.start('web', 80) do |http|
  req = Net::HTTP::Get.new('/')
  req.basic_auth 'test', 'test'
  http.request(req)
end

puts "response code: '#{res.code}'"
puts "body is: '#{res.body}'"
